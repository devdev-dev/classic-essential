package com.deviantdev.wear

import android.content.res.Resources

/** Converts dp to pixel */
val Float.dpToPx: Float get() = (this * Resources.getSystem().displayMetrics.density)

/** Converts pixel to dp */
val Float.pxToDp: Float get() = (this / Resources.getSystem().displayMetrics.density)