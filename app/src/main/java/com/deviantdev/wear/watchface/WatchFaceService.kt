package com.deviantdev.wear.watchface

import android.graphics.Canvas
import android.graphics.PointF
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.wearable.watchface.CanvasWatchFaceService
import android.support.wearable.watchface.WatchFaceService
import android.support.wearable.watchface.WatchFaceStyle
import android.view.SurfaceHolder
import com.jakewharton.threetenabp.AndroidThreeTen
import java.lang.ref.WeakReference

/**
 * Analog watch face with a ticking second hand. In ambient mode, the second hand isn't
 * shown. On devices with low-bit ambient mode, the hands are drawn without anti-aliasing in ambient
 * mode. The watch face is drawn with less contrast in mute mode.
 */
class WatchFaceService : CanvasWatchFaceService() {

    override fun onCreateEngine(): Engine {
        AndroidThreeTen.init(this)
        return Engine()
    }

    private class EngineHandler(reference: Engine) : Handler() {
        private val mWeakReference: WeakReference<Engine> = WeakReference(reference)

        override fun handleMessage(msg: Message) {
            val engine = mWeakReference.get()
            if (engine != null) {
                when (msg.what) {
                    MSG_UPDATE_TIME -> engine.handleUpdateTimeMessage()
                }
            }
        }
    }

    inner class Engine : CanvasWatchFaceService.Engine() {

        private lateinit var watchFacePreferences: WatchFacePreferences
        private lateinit var watchFacePaintUtil: WatchFacePaintUtil
        private lateinit var watchFaceDrawUtil: WatchFaceDrawUtil

        private var mAmbient: Boolean = false
        private var mLowBitAmbient: Boolean = false
        private var mBurnInProtection: Boolean = false

        /* Handler to update the time once a second in interactive mode. */
        private val mUpdateTimeHandler = EngineHandler(this)

        override fun onCreate(holder: SurfaceHolder) {
            super.onCreate(holder)
            setWatchFaceStyle(
                    WatchFaceStyle.Builder(this@WatchFaceService).setAcceptsTapEvents(true).build())

            watchFacePreferences = WatchFacePreferences(applicationContext)
            watchFacePaintUtil = WatchFacePaintUtil(applicationContext, watchFacePreferences)
            watchFaceDrawUtil = WatchFaceDrawUtil(applicationContext, watchFacePaintUtil)
        }

        override fun onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            super.onDestroy()
        }

        override fun onPropertiesChanged(properties: Bundle) {
            super.onPropertiesChanged(properties)
            mLowBitAmbient = properties.getBoolean(WatchFaceService.PROPERTY_LOW_BIT_AMBIENT, false)
            mBurnInProtection =
                    properties.getBoolean(WatchFaceService.PROPERTY_BURN_IN_PROTECTION, false)
        }

        override fun onTimeTick() {
            super.onTimeTick()
            invalidate()
        }

        override fun onAmbientModeChanged(inAmbientMode: Boolean) {
            super.onAmbientModeChanged(inAmbientMode)
            watchFacePaintUtil.inAmbientMode = inAmbientMode
            watchFaceDrawUtil.inAmbientMode = inAmbientMode
            watchFaceDrawUtil.showBattery = false

            // Check and trigger whether or not timer should be running (only in active mode).
            updateTimer()
        }

        override fun onInterruptionFilterChanged(interruptionFilter: Int) {
            super.onInterruptionFilterChanged(interruptionFilter)
            val inMuteMode = interruptionFilter == WatchFaceService.INTERRUPTION_FILTER_NONE
            watchFacePaintUtil.inMuteMode = inMuteMode
            invalidate()
        }

        override fun onSurfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            super.onSurfaceChanged(holder, format, width, height)

            val center = PointF(width / 2f, height / 2f)
            watchFaceDrawUtil.center = center
            watchFacePaintUtil.center = center
        }

        private var batteryResetHandler = Handler()

        override fun onTapCommand(tapType: Int, x: Int, y: Int, eventTime: Long) {
            when (tapType) {
                WatchFaceService.TAP_TYPE_TAP -> {
                    watchFaceDrawUtil.showBattery = !watchFaceDrawUtil.showBattery
                    if (watchFaceDrawUtil.showBattery) {
                        batteryResetHandler.postDelayed({
                            watchFaceDrawUtil.showBattery = false
                            invalidate()
                        }, 5000)
                    } else {
                        batteryResetHandler.removeCallbacksAndMessages(null)
                    }
                }
                else -> super.onTapCommand(tapType, x, y, eventTime)
            }
            invalidate()
        }

        override fun onDraw(canvas: Canvas, bounds: Rect) {
            watchFaceDrawUtil.draw(canvas)
        }

        override fun onVisibilityChanged(visible: Boolean) {
            super.onVisibilityChanged(visible)

            /* Check and trigger whether or not timer should be running (only in active mode). */
            updateTimer()
        }

        /**
         * Starts/stops the [.mUpdateTimeHandler] timer based on the state of the watch face.
         */
        private fun updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
            }
        }

        /**
         * Returns whether the [.mUpdateTimeHandler] timer should be running. The timer
         * should only run in active mode.
         */
        private fun shouldTimerBeRunning(): Boolean {
            return isVisible && !mAmbient
        }

        /**
         * Handle updating the time periodically in interactive mode.
         */
        fun handleUpdateTimeMessage() {
            invalidate()
            if (shouldTimerBeRunning()) {
                val timeMs = System.currentTimeMillis()
                val delayMs = INTERACTIVE_UPDATE_RATE_MS - timeMs % INTERACTIVE_UPDATE_RATE_MS
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs)
            }
        }
    }

    companion object {
        private val TAG = WatchFaceService::class.java.simpleName!!

        /** Updates rate in milliseconds for interactive mode. */
        private var INTERACTIVE_UPDATE_RATE_MS = 1000

        /** Handler message id for updating the time periodically in interactive mode. */
        private const val MSG_UPDATE_TIME = 0
    }
}


