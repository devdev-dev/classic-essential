package com.deviantdev.wear.watchface

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import android.graphics.Rect
import android.os.BatteryManager
import com.deviantdev.wear.R
import com.deviantdev.wear.dpToPx
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

class WatchFaceDrawUtil(context: Context, private val watchFacePaintUtil: WatchFacePaintUtil) {

    private val batteryManager = context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
    private val locale = context.resources.configuration.locales[0]

    private val batteryHeight = context.resources.getDimension(R.dimen.batteryHeight)
    private val batteryPadding = context.resources.getDimension(R.dimen.batteryPadding)
    private val batteryOuterWidth = context.resources.getDimension(R.dimen.batteryWidth)
    private val batteryInnerWidth = batteryOuterWidth - batteryPadding * 2

    private val dayTextHeight = context.resources.getDimension(R.dimen.dayTextHeight)
    private val monthTextHeight = context.resources.getDimension(R.dimen.monthTextHeight)
    private val circleOuterRadius = context.resources.getDimension(R.dimen.circleOuterRadius)
    private val circleInnerRadius = context.resources.getDimension(R.dimen.circleInnerRadius)

    private var informationPosition: Long = BOTTOM

    var center = PointF(0F, 0F)

    private var now = LocalDateTime.now()

    var inAmbientMode = false

    private var secondsRotation = 0F
    private var minutesRotation = 0F
    private var hoursRotation = 0F

    private var batteryPercent = 0
    private var actualBatteryWidth = 0F

    var showBattery = false
        set(value) {
            field = value
            batteryPercent = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
            actualBatteryWidth = (batteryInnerWidth / 100F) * batteryPercent
        }

    fun draw(canvas: Canvas) {
        showBattery = showBattery || batteryManager.isCharging

        now = LocalDateTime.now()

        /* These calculations reflect the rotation in degrees per unit of time, e.g. */
        secondsRotation = (now.second * 6f) % 360
        minutesRotation = (now.minute * 6f) % 360
        hoursRotation = (now.hour * 30f) % 360 + (now.minute / 2f) % 360

        drawBackground(canvas)
        drawWatchHands(canvas)
    }

    private fun drawBackground(canvas: Canvas) {
        canvas.drawColor(watchFacePaintUtil.backgroundColor)

        for (tickIndex in 0..59) { // watch face ticks
            if (tickIndex % 5 == 0) {
                drawDialMarker(canvas, tickIndex, center.x * 0.175F,
                        watchFacePaintUtil.getHourMarkerPaint())
            } else {
                drawDialMarker(canvas, tickIndex, center.x * 0.0625F,
                        watchFacePaintUtil.getMinuteMarkerPaint())
            }
        }

        informationPosition = calculateDatePosition()
        if (showBattery) drawBattery(canvas) else drawDate(canvas)
    }

    private fun drawDialMarker(canvas: Canvas, tickIndex: Int, markerLength: Float, paint: Paint) {
        val innerHourRadius = center.x - markerLength
        val outerHourRadius = center.x - 0

        val tickRot = (tickIndex.toDouble() * Math.PI * 2.0 / 60).toFloat()

        val innerPoint = PointF(center.x + Math.sin(tickRot.toDouble()).toFloat() * innerHourRadius,
                center.y + (-Math.cos(tickRot.toDouble()).toFloat()) * innerHourRadius)
        val outerPoint = PointF(center.x + Math.sin(tickRot.toDouble()).toFloat() * outerHourRadius,
                center.y + (-Math.cos(tickRot.toDouble())).toFloat() * outerHourRadius)

        canvas.drawLine(innerPoint.x, innerPoint.y, outerPoint.x, outerPoint.y, paint)

    }

    private val textBoundaryRect = Rect()

    private fun calculateDatePosition(): Long {
        if (inUpperHalf(hoursRotation) && inUpperHalf(minutesRotation)) return BOTTOM
        if (!inUpperHalf(hoursRotation) && !inUpperHalf(minutesRotation)) return TOP
        if (inLeftHalf(hoursRotation) && inLeftHalf(minutesRotation)) return RIGHT
        if (!inLeftHalf(hoursRotation) && !inLeftHalf(minutesRotation)) return LEFT

        val hourGravity = getGravityForDegree(hoursRotation)
        val minutesGravity = getGravityForDegree(minutesRotation)

        return if (hourGravity != BOTTOM && minutesGravity != BOTTOM) BOTTOM
        else if (hourGravity != TOP && minutesGravity != TOP) TOP
        else if (hourGravity != RIGHT && minutesGravity != RIGHT) RIGHT
        else LEFT
    }

    private fun inUpperHalf(degree: Float) = degree > 270 || degree < 90

    private fun inLeftHalf(degree: Float) = degree > 180

    private fun getGravityForDegree(degree: Float): Long {
        return if (degree >= 315 || degree < 45) TOP
        else if (degree >= 45 && degree < 135) RIGHT
        else if (degree >= 225 && degree < 315) LEFT
        else BOTTOM
    }

    private fun drawDate(canvas: Canvas) {

        val dayString = now.format(DateTimeFormatter.ofPattern("EE dd").withLocale(locale))
        val dayTextPaint = watchFacePaintUtil.getDayTextPaint()
        dayTextPaint.getTextBounds(dayString, 0, dayString.length, textBoundaryRect)
        val dayPos = PointF(center.x - textBoundaryRect.width() / 2f - textBoundaryRect.left,
                center.y + textBoundaryRect.height() / 2f - textBoundaryRect.bottom - dayTextHeight / 2)

        val monthString =
                now.format(DateTimeFormatter.ofPattern("LLLL").withLocale(locale)).toUpperCase()
        val monthTextPaint = watchFacePaintUtil.getMonthTextPaint()
        monthTextPaint.getTextBounds(monthString, 0, monthString.length, textBoundaryRect)
        val monthPos = PointF(center.x - textBoundaryRect.width() / 2f - textBoundaryRect.left,
                center.y + textBoundaryRect.height() / 2f - textBoundaryRect.bottom + monthTextHeight / 2)

        adjustOriginToPosition(dayPos)
        adjustOriginToPosition(monthPos)

        canvas.drawText(dayString, dayPos.x, dayPos.y, dayTextPaint)
        canvas.drawText(monthString, monthPos.x, monthPos.y, monthTextPaint)
    }

    private fun adjustOriginToPosition(origin: PointF) {
        when (informationPosition) {
            TOP -> origin.y -= center.y / 2
            RIGHT -> origin.x += center.x / 2
            BOTTOM -> origin.y += center.y / 2
            LEFT -> origin.x -= center.x / 2
        }
    }

    private fun drawBattery(canvas: Canvas) {

        val batteryPaint = watchFacePaintUtil.getBatteryPaint(true)

        val tl = PointF(center.x - batteryOuterWidth / 2, center.y - batteryHeight / 2)
        val br = PointF(center.x + batteryOuterWidth / 2, center.y + batteryHeight / 2)

        adjustOriginToPosition(tl)
        adjustOriginToPosition(br)

        canvas.drawRect(tl.x, tl.y, br.x, br.y, batteryPaint)

        batteryPaint.style = Paint.Style.FILL

        canvas.drawRect(br.x, tl.y + batteryHeight / 4, br.x + batteryHeight / 5,
                br.y - batteryHeight / 4, batteryPaint)

        val batteryWidth = //
                if (batteryManager.isCharging) //
                    if ((now.second % 2) == 1) batteryInnerWidth else actualBatteryWidth
                else actualBatteryWidth

        if (batteryPercent < 15) batteryPaint.apply { color = Color.RED; shader = null }

        canvas.drawRect(tl.x + batteryPadding, tl.y + batteryPadding,
                tl.x + batteryPadding + batteryWidth, br.y - batteryPadding, batteryPaint)
    }

    private fun drawWatchHands(canvas: Canvas) {

        /* Save the canvas state before we can begin to rotate it and later restore it. */
        canvas.save()

        drawHourHand(canvas, hoursRotation)
        drawMinuteHand(canvas, minutesRotation, hoursRotation)
        drawSecondHand(canvas, secondsRotation, minutesRotation)

        canvas.restore()

        canvas.drawCircle(center.x, center.y, circleOuterRadius,
                watchFacePaintUtil.getCircleOuterPaint())
        canvas.drawCircle(center.x, center.y, circleInnerRadius,
                watchFacePaintUtil.getCircleInnerPaint())
    }

    private fun drawHourHand(canvas: Canvas, hoursRotation: Float) {
        canvas.rotate(hoursRotation, center.x, center.y)

        val handOffset = center.y * 0.125F
        val hourHandLengthBase = (center.x * 0.50F)
        val hourHandLengthTip = (center.x * 0.55F)

        canvas.drawLine(center.x, center.y, center.x, center.y - handOffset,
                watchFacePaintUtil.getHourHandPaint())

        val path = Path()
        path.moveTo(center.x + 1.5F.dpToPx, center.y - handOffset)
        path.lineTo(center.x + 1.5F.dpToPx, center.y - hourHandLengthBase)
        path.lineTo(center.x + 0.0F.dpToPx, center.y - hourHandLengthTip)
        path.lineTo(center.x - 1.5F.dpToPx, center.y - hourHandLengthBase)
        path.lineTo(center.x - 1.5F.dpToPx, center.y - handOffset)
        path.close()

        canvas.drawPath(path, watchFacePaintUtil.getHourHandPaint())
    }

    private fun drawMinuteHand(canvas: Canvas, minutesRotation: Float, hoursRotation: Float) {
        canvas.rotate(minutesRotation - hoursRotation, center.x, center.y)

        val handOffset = center.y * 0.125F
        val minuteHandLengthBase = (center.x * 0.825F)
        val minuteHandLengthTip = (center.x * 0.875F)

        canvas.drawLine(center.x, center.y, center.x, center.y - handOffset,
                watchFacePaintUtil.getMinuteHandPaint())

        val path = Path()
        path.moveTo(center.x + 1.5F.dpToPx, center.y - handOffset)
        path.lineTo(center.x + 1.5F.dpToPx, center.y - minuteHandLengthBase)
        path.lineTo(center.x + 0.0F.dpToPx, center.y - minuteHandLengthTip)
        path.lineTo(center.x - 1.5F.dpToPx, center.y - minuteHandLengthBase)
        path.lineTo(center.x - 1.5F.dpToPx, center.y - handOffset)
        path.close()

        canvas.drawPath(path, watchFacePaintUtil.getMinuteHandPaint())
    }

    /*
     * Ensure the "seconds" hand is drawn only when we are in interactive mode.
     * Otherwise, we only update the watch face once a minute.
     */
    private fun drawSecondHand(canvas: Canvas, secondsRotation: Float, minutesRotation: Float) {
        if (!watchFacePaintUtil.inAmbientMode) {

            canvas.rotate(secondsRotation - minutesRotation, center.x, center.y)

            val secondHandLength = (center.x * 0.875F)
            canvas.drawLine(center.x, center.y, center.x, center.y - secondHandLength,
                    watchFacePaintUtil.getSecondHandPaint())
        }
    }

    companion object {
        val TAG: String = WatchFaceDrawUtil::class.java.simpleName!!

        const val TOP = 0L
        const val RIGHT = 1L
        const val BOTTOM = 2L
        const val LEFT = 3L
    }
}