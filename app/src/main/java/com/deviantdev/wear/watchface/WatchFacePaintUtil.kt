package com.deviantdev.wear.watchface

import android.content.Context
import android.graphics.Color
import android.graphics.CornerPathEffect
import android.graphics.Paint
import android.graphics.PointF
import android.graphics.RadialGradient
import android.graphics.Shader
import com.deviantdev.wear.R
import com.deviantdev.wear.dpToPx

class WatchFacePaintUtil(context: Context, private val watchFacePreferences: WatchFacePreferences) {

    init {
        watchFacePreferences.addBaseColorChangeListener(
                { baseColor = watchFacePreferences.baseColor; refreshAllPaints() })
        watchFacePreferences.addSecondHandColorChangeListener(
                { secondHandColor = watchFacePreferences.secondHandColor; refreshAllPaints() })
        watchFacePreferences.addBackgroundColorChangeListener(
                { backgroundColor = watchFacePreferences.backgroundColor; refreshAllPaints() })
        watchFacePreferences.addGradientEnabledChangeListener({ refreshAllPaints() })
    }

    private var baseColor = watchFacePreferences.baseColor
        set(value) {
            field = value
            baseColorDarker = manipulateColor(baseColor, 0.5f)
            baseColorLighter = manipulateColor(baseColor, 1.75f)
            refreshAllPaints()
        }
    private var baseColorDarker = manipulateColor(baseColor, 0.5f)
    private var baseColorLighter = manipulateColor(baseColor, 1.75f)

    private var secondHandColor = watchFacePreferences.secondHandColor
        set(value) {
            field = value
            secondHandColorDarker = manipulateColor(secondHandColor, 0.5f)
            secondHandColorLighter = manipulateColor(secondHandColor, 1.75f)
            refreshAllPaints()
        }
    private var secondHandColorDarker = manipulateColor(secondHandColor, 0.5f)
    private var secondHandColorLighter = manipulateColor(secondHandColor, 1.75f)

    private var baseColorAmbient = Color.LTGRAY
    private var baseColorDarkerAmbient = manipulateColor(baseColorAmbient, 0.5f)
    private var baseColorLighterAmbient = manipulateColor(baseColorAmbient, 1.75f)

    private val hourMarkerWidth = context.resources.getDimension(R.dimen.hourMarkerWidth)
    private val minuteMarkerWidth = context.resources.getDimension(R.dimen.minuteMarkerWidth)

    private val dayTextHeight = context.resources.getDimension(R.dimen.dayTextHeight)
    private val monthTextHeight = context.resources.getDimension(R.dimen.monthTextHeight)

    // re-initialize all paints if the center was changed
    var center = PointF(0F, 0F)
        set(value) {
            field = value
            refreshAllPaints()
        }

    var inAmbientMode = false
        set(value) {
            field = value
            refreshAllPaints()
        }

    var inMuteMode = false
        set(value) {
            field = value
            refreshAllPaints()
        }

    var backgroundColor = Color.BLACK
        get() = if (inAmbientMode && !watchFacePreferences.colorfulAmbient) Color.BLACK else watchFacePreferences.backgroundColor

    private var hourHandPaint = Paint()
    fun getHourHandPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            hourHandPaint = Paint().apply {
                color =
                        if (inAmbientMode && !watchFacePreferences.colorfulAmbient) baseColorLighterAmbient else baseColor
                strokeWidth = 1F.dpToPx
                strokeCap = Paint.Cap.ROUND
                pathEffect = CornerPathEffect(5F)
                style = Paint.Style.FILL

                alpha = if (inMuteMode) 100 else 255

                isAntiAlias = !inAmbientMode
                if ((!inAmbientMode || watchFacePreferences.colorfulAmbient) && watchFacePreferences.gradientEnabled) {
                    shader = RadialGradient(center.x, center.y, center.x, baseColorDarker,
                            baseColorLighter, Shader.TileMode.CLAMP)
                }
            }
        }
        return hourHandPaint
    }

    private var minuteHandPaint = Paint()
    fun getMinuteHandPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            minuteHandPaint = Paint().apply {
                color =
                        if (inAmbientMode && !watchFacePreferences.colorfulAmbient) baseColorLighterAmbient else baseColor
                strokeWidth = 1F.dpToPx
                strokeCap = Paint.Cap.ROUND
                pathEffect = CornerPathEffect(5F)
                style = Paint.Style.FILL

                alpha = if (inMuteMode) 100 else 255

                isAntiAlias = !inAmbientMode
                if ((!inAmbientMode || watchFacePreferences.colorfulAmbient) && watchFacePreferences.gradientEnabled) {
                    shader = RadialGradient(center.x, center.y, center.x, baseColorDarker,
                            baseColorLighter, Shader.TileMode.CLAMP)
                }
            }
        }
        return minuteHandPaint
    }

    private var secondHandPaint = Paint()
    fun getSecondHandPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            secondHandPaint = Paint().apply {

                color =
                        if (inAmbientMode && !watchFacePreferences.colorfulAmbient) baseColorLighterAmbient else secondHandColor

                strokeWidth = 1F.dpToPx
                strokeCap = Paint.Cap.ROUND
                style = Paint.Style.FILL

                alpha = if (inMuteMode) 100 else 255

                isAntiAlias = !inAmbientMode
                if ((!inAmbientMode || watchFacePreferences.colorfulAmbient) && watchFacePreferences.gradientEnabled) {
                    shader = RadialGradient(center.x, center.y, center.x, secondHandColorDarker,
                            secondHandColor, Shader.TileMode.CLAMP)
                }
            }
        }
        return secondHandPaint
    }

    private var circleOuterPaint = Paint()
    fun getCircleOuterPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            circleOuterPaint = Paint().apply {
                color =
                        if (inAmbientMode && !watchFacePreferences.colorfulAmbient) Color.WHITE else baseColorDarker
                strokeWidth = hourMarkerWidth
                style = Paint.Style.FILL

                isAntiAlias = !inAmbientMode
            }
        }
        return circleOuterPaint
    }

    private var circleInnerPaint = Paint()
    fun getCircleInnerPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            circleInnerPaint = Paint().apply {
                color = Color.BLACK
                strokeWidth = hourMarkerWidth
                style = Paint.Style.FILL

                isAntiAlias = !inAmbientMode
            }
        }
        return circleInnerPaint
    }

    private var hourMarkerPaint = Paint()
    fun getHourMarkerPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            hourMarkerPaint = Paint().apply {
                color =
                        if (inAmbientMode && !watchFacePreferences.colorfulAmbient) baseColorDarkerAmbient else baseColor
                strokeWidth = hourMarkerWidth
                style = Paint.Style.STROKE

                isAntiAlias = !inAmbientMode
                if ((!inAmbientMode || watchFacePreferences.colorfulAmbient) && watchFacePreferences.gradientEnabled) {
                    shader = RadialGradient(center.x, center.y, center.x, baseColorLighter,
                            baseColorDarker, Shader.TileMode.CLAMP)
                }
            }
        }
        return hourMarkerPaint
    }

    private var minuteMarkerPaint = Paint()
    fun getMinuteMarkerPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            minuteMarkerPaint = Paint().apply {
                color =
                        if (inAmbientMode && !watchFacePreferences.colorfulAmbient) baseColorDarkerAmbient else baseColor
                strokeWidth = minuteMarkerWidth
                style = Paint.Style.STROKE

                isAntiAlias = !inAmbientMode
                if ((!inAmbientMode || watchFacePreferences.colorfulAmbient) && watchFacePreferences.gradientEnabled) {
                    shader = RadialGradient(center.x, center.y, center.x, baseColorLighter,
                            baseColorDarker, Shader.TileMode.CLAMP)
                }
            }
        }
        return minuteMarkerPaint
    }

    private var dayTextPaint = Paint()
    fun getDayTextPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            dayTextPaint = Paint().apply {
                color =
                        if (inAmbientMode && !watchFacePreferences.colorfulAmbient) baseColorAmbient else baseColor
                textSize = dayTextHeight

                isAntiAlias = !inAmbientMode
                if ((!inAmbientMode || watchFacePreferences.colorfulAmbient) && watchFacePreferences.gradientEnabled) {
                    shader = RadialGradient(center.x, center.y, center.x, baseColorDarker,
                            baseColorLighter, Shader.TileMode.CLAMP)
                }
            }
        }
        return dayTextPaint
    }

    private var monthTextPaint = Paint()
    fun getMonthTextPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            monthTextPaint = Paint().apply {
                color =
                        if (inAmbientMode && !watchFacePreferences.colorfulAmbient) baseColorAmbient else baseColor
                textSize = monthTextHeight

                isAntiAlias = !inAmbientMode
                if ((!inAmbientMode || watchFacePreferences.colorfulAmbient) && watchFacePreferences.gradientEnabled) {
                    shader = RadialGradient(center.x, center.y, center.x, baseColorDarker,
                            baseColorLighter, Shader.TileMode.CLAMP)
                }
            }
        }
        return monthTextPaint
    }

    private var batteryPaint = Paint()
    fun getBatteryPaint(refresh: Boolean = false): Paint {
        if (refresh) {
            batteryPaint = Paint().apply {
                color =
                        if (inAmbientMode && !watchFacePreferences.colorfulAmbient) baseColorAmbient else baseColor
                style = Paint.Style.STROKE

                isAntiAlias = !inAmbientMode
                if ((!inAmbientMode || watchFacePreferences.colorfulAmbient) && watchFacePreferences.gradientEnabled) {
                    shader = RadialGradient(center.x, center.y, center.x, baseColorDarker,
                            baseColorLighter, Shader.TileMode.CLAMP)
                }
            }
        }
        return batteryPaint
    }

    private fun refreshAllPaints() {
        getHourHandPaint(true)
        getMinuteHandPaint(true)
        getSecondHandPaint(true)
        getHourMarkerPaint(true)
        getMinuteMarkerPaint(true)
        getCircleInnerPaint(true)
        getCircleOuterPaint(true)
        getDayTextPaint(true)
        getMonthTextPaint(true)
        getBatteryPaint(true)
    }

    private fun manipulateColor(color: Int, factor: Float): Int {
        val hsv = FloatArray(3)
        Color.colorToHSV(color, hsv)
        hsv[2] *= factor
        return Color.HSVToColor(hsv)
    }
}