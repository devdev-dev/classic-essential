package com.deviantdev.wear.watchface.migration

import android.content.SharedPreferences
import com.deviantdev.wear.BuildConfig
import com.deviantdev.wear.watchface.WatchFacePreferences

private const val VERSION_CODE_1_3_0 = 1030000

fun handlePotentialVersionUpdate(sharedPreferences: SharedPreferences) {
    if (isVersionUpdated(sharedPreferences)) {
        updatePreferences(sharedPreferences)
    }
}

private fun isVersionUpdated(sharedPreferences: SharedPreferences) =
        sharedPreferences.getInt(WatchFacePreferences.VERSION_CODE, 0) != BuildConfig.VERSION_CODE

private fun updatePreferences(sharedPreferences: SharedPreferences) {
    val edit = sharedPreferences.edit()
    edit.putInt(WatchFacePreferences.VERSION_CODE, BuildConfig.VERSION_CODE)
    edit.apply()
}