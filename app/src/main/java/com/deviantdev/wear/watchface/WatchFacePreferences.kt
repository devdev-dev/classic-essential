package com.deviantdev.wear.watchface

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.graphics.Color
import com.deviantdev.wear.watchface.migration.handlePotentialVersionUpdate

class WatchFacePreferences(context: Context) : OnSharedPreferenceChangeListener {

    private val sharedPreferences: SharedPreferences
    private val listeners = mutableMapOf<String, MutableList<() -> Unit>>()

    var baseColor
        get() = sharedPreferences.getInt(BASE_COLOR, Color.LTGRAY)
        set(value) {
            val edit = sharedPreferences.edit()
            edit.putInt(BASE_COLOR, value)
            edit.apply()
        }

    var secondHandColor
        get() = sharedPreferences.getInt(SECOND_HAND_COLOR, Color.RED)
        set(value) {
            val edit = sharedPreferences.edit()
            edit.putInt(SECOND_HAND_COLOR, value)
            edit.apply()
        }

    var backgroundColor
        get() = sharedPreferences.getInt(BACKGROUND_COLOR, Color.BLACK)
        set(value) {
            val edit = sharedPreferences.edit()
            edit.putInt(BACKGROUND_COLOR, value)
            edit.apply()
        }

    var gradientEnabled
        get() = sharedPreferences.getBoolean(GRADIENT_ENABLED, true)
        set(value) {
            val edit = sharedPreferences.edit()
            edit.putBoolean(GRADIENT_ENABLED, value)
            edit.apply()
        }

    var colorfulAmbient
        get() = sharedPreferences.getBoolean(COLORFUL_AMBIENT, false)
        set(value) {
            val edit = sharedPreferences.edit()
            edit.putBoolean(COLORFUL_AMBIENT, value)
            edit.apply()
        }

    init {
        sharedPreferences = context.getSharedPreferences(PREFERENCE_FILE_KEY, Context.MODE_PRIVATE)
        sharedPreferences.registerOnSharedPreferenceChangeListener(this)

        handlePotentialVersionUpdate(sharedPreferences)
    }

    fun addBaseColorChangeListener(action: () -> Unit) {
        if (listeners.containsKey(BASE_COLOR)) {
            val list: MutableList<() -> Unit> = listeners[BASE_COLOR]!!
            list.add(action)
            listeners[BASE_COLOR] = list
        } else {
            listeners[BASE_COLOR] = mutableListOf(action)
        }
    }

    fun addSecondHandColorChangeListener(action: () -> Unit) {
        if (listeners.containsKey(SECOND_HAND_COLOR)) {
            val list: MutableList<() -> Unit> = listeners[SECOND_HAND_COLOR]!!
            list.add(action)
            listeners[SECOND_HAND_COLOR] = list
        } else {
            listeners[SECOND_HAND_COLOR] = mutableListOf(action)
        }
    }

    fun addBackgroundColorChangeListener(action: () -> Unit) {
        if (listeners.containsKey(BACKGROUND_COLOR)) {
            val list: MutableList<() -> Unit> = listeners[BACKGROUND_COLOR]!!
            list.add(action)
            listeners[BACKGROUND_COLOR] = list
        } else {
            listeners[BACKGROUND_COLOR] = mutableListOf(action)
        }
    }

    fun addGradientEnabledChangeListener(action: () -> Unit) {
        if (listeners.containsKey(GRADIENT_ENABLED)) {
            val list: MutableList<() -> Unit> = listeners[GRADIENT_ENABLED]!!
            list.add(action)
            listeners[GRADIENT_ENABLED] = list
        } else {
            listeners[GRADIENT_ENABLED] = mutableListOf(action)
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        listeners[key]?.let { it.forEach { it() } }
    }

    fun resetToDefaults() {
        baseColor = Color.LTGRAY
        secondHandColor = Color.RED
        backgroundColor = Color.BLACK
        colorfulAmbient = false
        gradientEnabled = true
    }

    companion object {
        private val TAG = WatchFacePreferences::class.java.simpleName!!

        const val PREFERENCE_FILE_KEY = "com.deviantdev.wear.PREFERENCE_FILE_KEY_88533996411"

        const val VERSION_CODE = "version_code"

        const val BASE_COLOR = "base_color"
        const val SECOND_HAND_COLOR = "second_hand_color"
        const val BACKGROUND_COLOR = "background_color"

        const val GRADIENT_ENABLED = "has_gradient"
        const val COLORFUL_AMBIENT = "colorful_ambient"
    }
}