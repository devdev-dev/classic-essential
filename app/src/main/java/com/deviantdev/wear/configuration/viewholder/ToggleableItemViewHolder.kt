package com.deviantdev.wear.configuration.viewholder

import android.graphics.drawable.Drawable
import android.support.v4.content.res.ResourcesCompat
import android.view.View
import com.deviantdev.wear.R

class ToggleableItemViewHolder(view: View) : AbstractItemViewHolder(view), View.OnClickListener {

    private var drawableOptionOn: Drawable
    private val onString: String

    private var drawableOptionOff: Drawable
    private val offString: String

    var action: ((view: View, state: Boolean) -> Unit)? = null
    var state = true
        set(value) {
            field = value
            if (value) {
                super.setIcon(drawableOptionOn)
                textView.text = text + ":\n" + onString
            } else {
                super.setIcon(drawableOptionOff)
                textView.text = text + ":\n" + offString
            }
        }

    init {
        view.setOnClickListener(this)
        drawableOptionOn =
                ResourcesCompat.getDrawable(view.resources, R.drawable.ic_visibility_24dp, null)!!
        onString = view.resources.getString(R.string.config_activity_item_toggle_on)

        drawableOptionOff =
                ResourcesCompat.getDrawable(view.resources, R.drawable.ic_visibility_off_24dp,
                        null)!!
        offString = view.resources.getString(R.string.config_activity_item_toggle_off)

        state = true
    }

    override fun setIcon(drawable: Drawable) {
        throw Error("ToggleableItemViewHolder does not support external icon changes.")
    }

    override fun onClick(view: View) {
        val newState = !state
        action?.invoke(view, newState)
        state = newState
    }

    companion object {
        val TAG = ToggleableItemViewHolder::class.java.simpleName!!
    }
}