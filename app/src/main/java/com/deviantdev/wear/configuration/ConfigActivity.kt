package com.deviantdev.wear.configuration

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.wear.widget.WearableLinearLayoutManager
import com.deviantdev.wear.R.layout.activity_config
import com.deviantdev.wear.watchface.WatchFacePreferences
import kotlinx.android.synthetic.main.activity_config.*
import org.jraf.android.androidwearcolorpicker.app.ColorPickActivity

class ConfigActivity : Activity() {

    private val watchFacePreferences by lazy {
        WatchFacePreferences(applicationContext)
    }

    private val configActivityListAdapter by lazy {
        ConfigActivityListAdapter(this@ConfigActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_config)

        wearable_recycler_view.apply {
            isEdgeItemsCenteringEnabled = true

            layoutManager = WearableLinearLayoutManager(this@ConfigActivity,
                    CustomScrollingLayoutCallback())
            setHasFixedSize(true)

            adapter = configActivityListAdapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                BASE_COLOR_REQUEST_CODE -> watchFacePreferences.baseColor =
                        ColorPickActivity.getPickedColor(data)
                SECOND_HAND_COLOR_REQUEST_CODE -> watchFacePreferences.secondHandColor =
                        ColorPickActivity.getPickedColor(data)
                BACKGROUND_COLOR_REQUEST_CODE -> watchFacePreferences.backgroundColor =
                        ColorPickActivity.getPickedColor(data)
                COLORFUL_AMBIENT_REQUEST_CODE -> watchFacePreferences.colorfulAmbient = true
                RESET_WATCH_FACE_REQUEST_CODE -> watchFacePreferences.resetToDefaults()
            }
        }

        configActivityListAdapter.notifyDataSetChanged()
    }

    companion object {
        val TAG: String = ConfigActivity::class.java.simpleName!!

        const val BASE_COLOR_REQUEST_CODE = 1001
        const val SECOND_HAND_COLOR_REQUEST_CODE = 1005
        const val BACKGROUND_COLOR_REQUEST_CODE = 1002
        const val COLORFUL_AMBIENT_REQUEST_CODE = 1003
        const val RESET_WATCH_FACE_REQUEST_CODE = 1004

    }

}