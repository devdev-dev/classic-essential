package com.deviantdev.wear.configuration

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.ViewGroup
import com.deviantdev.wear.R
import com.deviantdev.wear.R.layout.activity_config_recycler_item
import com.deviantdev.wear.configuration.ConfigActivity.Companion.BACKGROUND_COLOR_REQUEST_CODE
import com.deviantdev.wear.configuration.ConfigActivity.Companion.BASE_COLOR_REQUEST_CODE
import com.deviantdev.wear.configuration.ConfigActivity.Companion.COLORFUL_AMBIENT_REQUEST_CODE
import com.deviantdev.wear.configuration.ConfigActivity.Companion.RESET_WATCH_FACE_REQUEST_CODE
import com.deviantdev.wear.configuration.ConfigActivity.Companion.SECOND_HAND_COLOR_REQUEST_CODE
import com.deviantdev.wear.configuration.ConfirmationActivity.Companion.EXTRA_INTENT_DESCRIPTION
import com.deviantdev.wear.configuration.viewholder.ClickableItemViewHolder
import com.deviantdev.wear.configuration.viewholder.ToggleableItemViewHolder
import com.deviantdev.wear.watchface.WatchFacePreferences
import org.jraf.android.androidwearcolorpicker.app.ColorPickActivity

class ConfigActivityListAdapter(
        private val activity: Activity) : RecyclerView.Adapter<ViewHolder>() {

    private val context = activity.applicationContext
    private val watchFacePreferences = WatchFacePreferences(context)
    private val resources = context.resources

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int {
        return itemList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {

            TYPE_BASE_COLOR -> {
                ClickableItemViewHolder(
                        LayoutInflater.from(context).inflate(activity_config_recycler_item, parent,
                                false))
            }

            TYPE_SECOND_HAND_COLOR -> {
                ClickableItemViewHolder(
                        LayoutInflater.from(context).inflate(activity_config_recycler_item, parent,
                                false))
            }

            TYPE_BACKGROUND_COLOR -> {
                ClickableItemViewHolder(
                        LayoutInflater.from(context).inflate(activity_config_recycler_item, parent,
                                false))
            }

            TYPE_COLORFUL_AMBIENT -> {
                ToggleableItemViewHolder(
                        LayoutInflater.from(context).inflate(activity_config_recycler_item, parent,
                                false))
            }

            TYPE_GRADIENTS -> {
                ToggleableItemViewHolder(
                        LayoutInflater.from(context).inflate(activity_config_recycler_item, parent,
                                false))
            }

            TYPE_RESET_WATCH_FACE -> {
                ClickableItemViewHolder(
                        LayoutInflater.from(context).inflate(activity_config_recycler_item, parent,
                                false))
            }
            else -> throw Error("Unexpected view type for ViewHolder creation.")
        }
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        when (viewHolder.itemViewType) {

            TYPE_BASE_COLOR -> {
                (viewHolder as ClickableItemViewHolder).text =
                        resources.getString(R.string.config_activity_item_base_color)
                viewHolder.color = watchFacePreferences.baseColor
                viewHolder.setIcon(
                        ResourcesCompat.getDrawable(resources, R.drawable.ic_brush_24dp, null)!!)
                viewHolder.action = { _ ->
                    val intent = ColorPickActivity.IntentBuilder()
                            .oldColor(watchFacePreferences.baseColor).build(context)
                    activity.startActivityForResult(intent, BASE_COLOR_REQUEST_CODE)
                }
            }

            TYPE_SECOND_HAND_COLOR -> {
                (viewHolder as ClickableItemViewHolder).text =
                        resources.getString(R.string.config_activity_item_second_hand_color)
                viewHolder.color = watchFacePreferences.secondHandColor
                viewHolder.setIcon(
                        ResourcesCompat.getDrawable(resources, R.drawable.ic_brush_24dp, null)!!)
                viewHolder.action = { _ ->
                    val intent = ColorPickActivity.IntentBuilder()
                            .oldColor(watchFacePreferences.secondHandColor).build(context)
                    activity.startActivityForResult(intent, SECOND_HAND_COLOR_REQUEST_CODE)
                }
            }

            TYPE_BACKGROUND_COLOR -> {
                (viewHolder as ClickableItemViewHolder).text =
                        resources.getString(R.string.config_activity_item_background_color)
                viewHolder.color = watchFacePreferences.backgroundColor
                viewHolder.setIcon(
                        ResourcesCompat.getDrawable(resources, R.drawable.ic_brush_24dp, null)!!)
                viewHolder.action = { _ ->
                    val intent = ColorPickActivity.IntentBuilder()
                            .oldColor(watchFacePreferences.backgroundColor).build(context)
                    activity.startActivityForResult(intent, BACKGROUND_COLOR_REQUEST_CODE)
                }
            }

            TYPE_COLORFUL_AMBIENT -> {
                (viewHolder as ToggleableItemViewHolder).text =
                        resources.getString(R.string.config_activity_item_colorful_ambient)
                viewHolder.color = Color.TRANSPARENT
                viewHolder.state = watchFacePreferences.colorfulAmbient
                viewHolder.action = { _, state ->
                    if (state) {
                        val intent = Intent(context, ConfirmationActivity::class.java)
                        intent.putExtra(EXTRA_INTENT_DESCRIPTION,
                                resources.getString(R.string.warning_colorful_ambient))
                        activity.startActivityForResult(intent, COLORFUL_AMBIENT_REQUEST_CODE)
                    } else {
                        watchFacePreferences.colorfulAmbient = false
                    }
                }
            }

            TYPE_GRADIENTS -> {
                (viewHolder as ToggleableItemViewHolder).text =
                        resources.getString(R.string.config_activity_item_gradients)
                viewHolder.color = Color.TRANSPARENT
                viewHolder.state = watchFacePreferences.gradientEnabled
                viewHolder.action = { _, state ->
                    watchFacePreferences.gradientEnabled = state
                }
            }

            TYPE_RESET_WATCH_FACE -> {
                (viewHolder as ClickableItemViewHolder).text =
                        resources.getString(R.string.config_activity_item_reset_watch_face)
                viewHolder.setIcon(
                        ResourcesCompat.getDrawable(resources, R.drawable.ic_reset, null)!!)
                viewHolder.color = Color.TRANSPARENT
                viewHolder.action = { _ ->
                    val intent = Intent(context, ConfirmationActivity::class.java)
                    intent.putExtra(EXTRA_INTENT_DESCRIPTION,
                            resources.getString(R.string.warning_reset_settings))
                    activity.startActivityForResult(intent, RESET_WATCH_FACE_REQUEST_CODE)
                }
            }
        }
    }

    companion object {
        private val TAG: String = ConfigActivityListAdapter::class.java.simpleName!!

        private const val TYPE_BASE_COLOR = 20
        private const val TYPE_SECOND_HAND_COLOR = 21
        private const val TYPE_BACKGROUND_COLOR = 30
        private const val TYPE_RESET_WATCH_FACE = 40
        private const val TYPE_COLORFUL_AMBIENT = 50
        private const val TYPE_GRADIENTS = 60
        private val itemList =
                listOf(TYPE_BACKGROUND_COLOR, TYPE_BASE_COLOR, TYPE_SECOND_HAND_COLOR,
                        TYPE_COLORFUL_AMBIENT, TYPE_GRADIENTS, TYPE_RESET_WATCH_FACE)

    }

}