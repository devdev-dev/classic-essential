package com.deviantdev.wear.configuration.viewholder

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.support.v7.widget.RecyclerView
import android.support.wear.widget.CircledImageView
import android.view.View
import android.widget.TextView
import com.deviantdev.wear.R
import com.deviantdev.wear.R.id.recycler_item_icon

open class AbstractItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    protected val textView = view.findViewById<TextView>(R.id.item_text)!!

    var text = ""
        set(value) {
            field = value
            textView.text = text
        }

    private val circledImageView = view.findViewById<CircledImageView>(R.id.item_circle)

    var color = Color.TRANSPARENT
        set(value) {
            field = value
            val shape: GradientDrawable =
                    (circledImageView.background as LayerDrawable).findDrawableByLayerId(
                            R.id.recycler_item_background) as GradientDrawable
            shape.mutate()
            shape.setColor(color)
        }

    open fun setIcon(drawable: Drawable) {
        (circledImageView.background as LayerDrawable).setDrawableByLayerId(recycler_item_icon,
                drawable)
    }

    companion object {
        val TAG = AbstractItemViewHolder::class.java.simpleName!!
    }
}