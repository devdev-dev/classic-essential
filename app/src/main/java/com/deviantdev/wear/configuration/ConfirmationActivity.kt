package com.deviantdev.wear.configuration

import android.app.Activity
import android.content.res.Resources
import android.os.Bundle
import com.deviantdev.wear.R
import kotlinx.android.synthetic.main.activity_confirmation.*

class ConfirmationActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)

        text_description.text = intent.getStringExtra(EXTRA_INTENT_DESCRIPTION)

        button_cancel.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        button_ok.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }

        adjustInset()
    }

    private fun adjustInset() {
        if (applicationContext.resources.configuration.isScreenRound) {
            val inset = (FACTOR * Resources.getSystem().displayMetrics.widthPixels).toInt()
            layout_content.setPadding(inset, inset, inset, inset)
        }
    }

    companion object {
        val TAG: String = ConfigActivity::class.java.simpleName!!

        private const val FACTOR = 0.146467f // c = a * sqrt(2)

        const val EXTRA_INTENT_DESCRIPTION = "EXTRA_INTENT_DESCRIPTION"
    }
}
