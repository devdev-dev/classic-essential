package com.deviantdev.wear.configuration.viewholder

import android.view.View

open class ClickableItemViewHolder(view: View) : AbstractItemViewHolder(view),
        View.OnClickListener {

    var action: ((view: View) -> Unit)? = null

    init {
        view.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        action?.invoke(view)
    }

    companion object {
        val TAG = ClickableItemViewHolder::class.java.simpleName!!
    }
}