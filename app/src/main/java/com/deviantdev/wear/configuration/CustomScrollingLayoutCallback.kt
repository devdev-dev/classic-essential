package com.deviantdev.wear.configuration

import android.support.v7.widget.RecyclerView
import android.support.wear.widget.WearableLinearLayoutManager
import android.view.View

class CustomScrollingLayoutCallback : WearableLinearLayoutManager.LayoutCallback() {

    private var mProgressToCenter: Float = 0.toFloat()

    override fun onLayoutFinished(child: View, parent: RecyclerView) {

        var yRelativeToCenterOffset = 0f

        if (parent.height.toFloat() > 0) {
            // Figure out % progress from top to bottom
            val centerOffset = child.height / 2.0f / parent.height.toFloat()
            yRelativeToCenterOffset = child.y / parent.height + centerOffset
        }

        // Normalize for center
        mProgressToCenter = Math.abs(0.5f - yRelativeToCenterOffset)
        // Adjust to the maximum scale
        mProgressToCenter = Math.min(mProgressToCenter, MAX_ICON_PROGRESS)

        child.scaleX = 1 - mProgressToCenter
        child.scaleY = 1 - mProgressToCenter
    }

    companion object {
        private const val MAX_ICON_PROGRESS = 0.75f
    }
}