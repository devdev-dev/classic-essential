# README #

### Description ###

* A classic watch face for Android wear 2.0+
* Built in two flavors: free (essential) and paid (advanced)

### Running/Debugging the watch face on physical devices ###

Connect Smartphone to PC and enable debugging on the watch and in the Android Wear app.
Run thistwo commands in a terminal on the PC:
* `adb forward tcp:4444 localabstract:/adb-hub`
* `adb connect 127.0.0.1:4444`

Source: [Android Developer Documentation](https://developer.android.com/training/wearables/apps/debugging.html#bt-watch)
